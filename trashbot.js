"use strict";
const { Client, Intents } = require('discord.js');
const client = new Client({ intents: [Intents.FLAGS.GUILD_MESSAGES] });
const {token} = require("./auth.json");

client.on('ready', function (evt) {
	console.log(`Connected as ${client.user.tag}`);
});

client.on('message', function (message) {
	let content = message.content.toLowerCase();
	let score = 0;
	if (content.search("nitro") != -1)
		++score;
	if (content.search("https://") != -1 || content.search("http://") != -1)
		++score;
	if(score > 1) {
		message.delete();
	}
});

client.login(token);
