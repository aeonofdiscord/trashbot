# trashbot

This is a very simple discord bot for automatically removing "free nitro" phishing links posted by compromised accounts. It's simple because the phishing bots are not very sophisticated.

You can install this script on your own server, or run the live version by clicking here:

https://discord.com/oauth2/authorize?client_id=902539697413292074&scope=bot&permissions=8192

and accepting the permissions. Currently the only thing the bot does is delete messages, so it only needs the "Manage messages" permission; in the future it might gain additional features.

## Installing trashbot on your own server

- Create a Discord application ID and bot, from here: https://discord.com/developers/applications/me
- Install the current stable version of Node: https://nodejs.org
- Check out the trashbot repository to your server.
- Run `npm install` to install dependencies.
- Create a file called `auth.json` in the same directory as the `trashbot.json` file. The contents should look like this:
```
{
    "token": "<your bot's authorisation token, provided by Discord>"
}
```

- Run `nohup node trashbot.json &`
- Authorise the bot on your Discord server by going to 
`https://discord.com/oauth2/authorize?client_id=YOUR_CLIENT_ID_HERE&scope=bot&permissions=8192`
- That should work!
